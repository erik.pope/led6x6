// Modified from https://create.arduino.cc/projecthub/SAnwandter1/programming-8x8-led-matrix-23475a
#include "font.h"

#define ROW_1 7
#define ROW_2 6
#define ROW_3 5
#define ROW_4 4
#define ROW_5 3
#define ROW_6 2


#define COL_1 8
#define COL_2 9
#define COL_3 10
#define COL_4 11
#define COL_5 12
#define COL_6 13

const byte rows[] = {
  ROW_1, ROW_2, ROW_3, ROW_4, ROW_5, ROW_6
};
const byte col[] = {
  COL_1, COL_2, COL_3, COL_4, COL_5, COL_6
};

void setup()
{
  // Open serial port
  Serial.begin(9600);

  // Set all used pins to OUTPUT
  for (byte i = 2; i <= 13; i++)
    pinMode(i, OUTPUT);

}

void loop() {

  //printMatrix("ARDUINO "); // Default 60 frames per Char

  //printMatrix("TEST ",  ); // Fast at 20 Frames per Char

  printMatrix("ABCDEFGHIJKLMNOPQRSTUVWXYZ! #", 150); // Slow at 100 Frames per Char
  
}



void  drawScreen(byte buffer2[])
{
  // Turn on each row in series
  for (byte i = 0; i < 6; i++)        // count next row
  {
    digitalWrite(col[i], HIGH);    //initiate whole row

    for (byte a = 0; a < 6; a++)    // count next row
    {
      // if You set (~buffer2[i] >> a) then You will have positive
      digitalWrite(rows[a], ((buffer2[i] >> a) & 0x01) - 0x01); // initiate whole column

      delayMicroseconds(300);       // uncoment deley for diferent speed of display
      //delayMicroseconds(1000);
      //delay(10);
      //delay(100);

      digitalWrite(rows[a], 1);      // reset whole column
    }

    digitalWrite(col[i], LOW);     // reset whole row
    // otherwise last row will intersect with next row
  }
}



byte getChar(char x) {

  byte charVal[6];

  switch (x) {
    case 'A':
      drawScreen(A);
      break;
    case 'B':
      drawScreen(B);
      break;
    case 'C':
      drawScreen(C);
      break;
    case 'D':
      drawScreen(D);
      break;
    case 'E':
      drawScreen(E);
      break;
    case 'F':
      drawScreen(F);
      break;
    case 'G':
      drawScreen(G);
      break;
    case 'H':
      drawScreen(H);
      break;
    case 'I':
      drawScreen(I);
      break;
    case 'J':
      drawScreen(J);
      break;
    case 'K':
      drawScreen(K);
      break;
    case 'L':
      drawScreen(L);
      break;
    case 'M':
      drawScreen(M);
      break;
    case 'N':
      drawScreen(N);
      break;
    case 'O':
      drawScreen(O);
      break;
    case 'P':
      drawScreen(P);
      break;
    case 'Q':
      drawScreen(Q);
      break;
    case 'R':
      drawScreen(R);
      break;
    case 'S':
      drawScreen(S);
      break;
    case 'T':
      drawScreen(T);
      break;
    case 'U':
      drawScreen(U);
      break;
    case 'V':
      drawScreen(V);
      break;
    case 'W':
      drawScreen(W);
      break;
    case 'X':
      drawScreen(X);
      break;
    case 'Y':
      drawScreen(Y);
      break;
    case 'Z':
      drawScreen(Z);
      break;
    case '!':
      drawScreen(EX);
      break;
    case ' ':
      drawScreen(BLANK);
      break;
    default:
      // if nothing else matches, do the default
      // default is optional
      drawScreen(ALL);
      break;
  }

  return charVal;
}


void printMatrix(String text) {
  for (auto x : text)
  {
    Serial.println(x);
    for (int i = 0; i <= 60; i++) {
      getChar(x);
    }
  }
}

void printMatrix(String text, int rate) {
  for (auto x : text)
  {
    Serial.println(x);
    for (int i = 0; i <= rate; i++) {
      getChar(x);
    }
  }
}
